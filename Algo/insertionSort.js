let data = [5, 2]

function insertionSort(inputData) {
    for (let i = 0; i < inputData.length; i++) {
        current = inputData[i];
        j = i - 1;

        while (j >= 0 && inputData[j] > current) {
            inputData[j + 1] = inputData[j];
            j -= 1;
        }
        inputData[j + 1] = current;
    }
    return inputData
}

insertionSort(data)