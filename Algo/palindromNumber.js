var isPalindrome = function (x) {
  if (x < 0) {
    return false;
  }
  let reverse = 0,
    y = x;

  while (y > 0) {
    const lastNumber = y % 10;
    reverse = reverse * 10 + lastNumber;
    y = (y / 10) | 0;
  }

  return x === reverse ? console.log(true) : console.log(false);
};

isPalindrome(123321);
