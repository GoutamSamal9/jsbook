const strs = ["dog", "racecar", "car"];
// const strs = ["flower", "flow", "flight"];
function myFunction() {
  let noPrefix = "";

  let commonPrefixData = [];
  let notCommonPrefixData = [];

  if (!strs.length) {
    return noPrefix;
  }

  let commonPrefix = strs[0].slice(0, 2);

  if (strs.length === 1) {
    console.log(commonPrefix);
  }

  let newArray = strs.slice(1);

  for (let i = 0; i < newArray.length; i++) {
    let comPairPrefix = newArray[i].slice(0, 2);
    if (commonPrefix === comPairPrefix) {
      commonPrefixData.push(commonPrefix);
    } else if (commonPrefix !== comPairPrefix) {
      notCommonPrefixData.push(comPairPrefix);
    }
  }

  if (commonPrefixData.length) {
    return commonPrefixData + noPrefix;
  } else if (notCommonPrefixData.length) {
    return noPrefix;
  }

  return strs.reduce((prev, next) => {
    "use strict";
    let i = 0;
    while (prev[i] && next[i] && prev[i] === next[i]) i++;
    // let value = prev.slice(0, i);
    // console.log(value);
    return prev.slice(0, i);
  });
}

myFunction();
