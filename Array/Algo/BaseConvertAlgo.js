class Stack {
  constructor() {
    this.item = [];
    this.count = 0;
  }
  // push the element into the stack
  push(element) {
    this.item[this.count] = element;
    this.count++;
    return this.count - 1;
  }

  // remove element from the stack
  pop() {
    if (this.count == 0) return undefined;
    let deleteItem = this.item[this.count - 1];
    this.count--;
    return deleteItem;
  }

  // check the top of the element in stack
  peek() {
    return this.item[this.count - 1];
  }

  // check if stack is empty or not
  isEmpty() {
    return this.count == 0;
  }

  // for check the size
  size() {
    return this.count;
  }

  // Print element in stack
  print() {
    let str = "";
    for (let i = 0; i < this.count; i++) {
      str += this.item[i] + "";
    }
    return str;
  }

  clear() {
    this.item = [];
    this.count = 0;
    return this.item;
  }
}

function baseConverter(decNumber, base) {
    var remStack = new Stack(), rem, baseString = '', digits = '0123456789ABCDEF';
    while (decNumber > 0) {
        rem = Math.floor(decNumber % base);
        remStack.push(rem);
        decNumber = Math.floor(decNumber / base);
    }
  while (!remStack.isEmpty()) {
        baseString += digits[remStack.pop()];
    }
    return baseString;
}

console.log(baseConverter(10,3));