class Node {
  constructor(data, next = null) {
    this.data = data;
    this.next = next;
  }
}

class LinkedList {
  constructor() {
    this.head = null;
    this.size = 0;
  }
  // insert first
  insertFirst(data) {
    this.head = new Node(data, this.head);
    this.size++;
  }

  // insertLast

  insertLast(data) {
    let node = new Node(data);
    let current;

    // if empty
    if (!this.head) {
      this.head = node;
    } else {
      current = this.head;

      while (current.next) {
        current = current.next;
      }
      current.next = node;
    }
    this.size++;
  }

  //insertAt
  insertAt(data, index) {
    //if index is out of range
    if (index > 0 && index > this.size) {
      return;
    }

    // if first index
    if (index === 0) {
      this.head = new Node(data, this.head);
      return;
    }

    const node = new Node(data);
    let current, previous;

    // set current to first

    current = this.head;
    let count = 0;

    while (count < index) {
      previous = current; //Node before index
      count++;
      current = current.next; //Node after
    }

    node.next = current;
    previous.next = node;
    // TODO: node is undefined

    this.size++;
  }

  // removing firstElement to the list

  removeFirstElement(index) {
    if (index > -1 && index > this.size) {
      return;
    }

    let current = this.head,
      previous,
      position = 0;

    if (index === 0) {
      this.head = current.next;
    } else {
      while (index++ < index) {
        previous = current;
        current = current.next;
      }
    }
  }

  // printList
  printListData() {
    let current = this.head;
    while (current) {
      console.log(current.data);
      current = current.next;
    }
  }
}

const list = new LinkedList();

list.insertFirst(100);
list.insertFirst(200);
list.insertFirst(300);
list.insertLast(400);
list.insertAt(500, 3);

list.printListData();
