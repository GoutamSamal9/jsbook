class Node {
    constructor(data, next=null){
        this.data=data;
        this.next=next;
    }
}

class LinkedList{
    constructor(){
        this.head=null;
        this.size=0;
    }
// insert element to linklist
    insertFirst(data){
        this.head=new Node(data,this.head);
        this.size++;
    }

// insert at last 
    insertLast(data){
        let node = new Node(data);
        let current;

        // if empty then make head
        if(!this.head){
            this.head=node
        }else{
            current=this.head;

            while(current.next){
                current=current.next;
            }
            current.next=node;
        }
        this.size++;

    }

// print list data 
    printListData(data){
        let current = this.head;

        while(current){
            console.log(current.data);
            current=current.next
        }
    }
}

const linkedList=new LinkedList();
linkedList.insertFirst(100);
linkedList.insertFirst(200);
linkedList.insertFirst(300);
linkedList.insertLast(400);

linkedList.printListData()