class Stack {
  constructor() {
    this.item = [];
    this.count = 0;
  }
  // push the element into the stack
  push(element) {
    this.item[this.count] = element;
    this.count++;
    return this.count - 1;
  }

  // remove element from the stack
  pop() {
    if (this.count == 0) return undefined;
    // console.log(this.item);
    let deleteItem = this.item[this.count - 1];
    this.count--;
    return deleteItem;
  }

  // check the top of the element in stack
  peek() {
    return this.item[this.count - 1];
  }

  // check if stack is empty or not
  isEmpty() {
    return this.count == 0;
  }

  // for check the size
  size() {
    return this.count;
  }

  // Print element in stack
  print() {
    let str = "";
    for (let i = 0; i < this.count; i++) {
      str += this.item[i] + "";
    }
    return str;
  }

  clear() {
    this.item = [];
    this.count = 0;
    return this.item;
  }
}

function dividedByTwo(decNumber) {
  var remStack = new Stack(),
    rem,
    binaryString = "";
  while (decNumber > 0) {
    rem = Math.floor(decNumber % 2);
    console.log(rem,"lll");
    remStack.push(rem);
    console.log(decNumber, "ooooo");
    
    decNumber = Math.floor(decNumber / 2);
    console.log(decNumber, "jjjjj");
  }
  while (!remStack.isEmpty()) {
    console.log(binaryString, "ggg");
    console.log(remStack.size(),"size");
    binaryString += remStack.pop().toString();
  }
  return binaryString;
  
}
console.log(dividedByTwo(11));