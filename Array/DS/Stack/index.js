class Stack {
  constructor() {
    this.item = [];
    this.count = 0;
  }
  // push the element into the stack
  push(element) {
    this.item[this.count] = element;
    console.log(element+' add to'+ this.count);
    this.count++;
    return this.count - 1;
  }

  // remove element from the stack
  pop() {
    if (this.count == 0) return undefined;
    console.log(this.item);
    let deleteItem = this.item[this.count - 1];
    console.log(deleteItem);
    this.count--;
    return deleteItem;
  }

  // check the top of the element in stack
  peek() {
    console.log(this.item[this.count - 1]);
    return this.item[this.count - 1];
  }

  // check if stack is empty or not
  isEmpty() {
    console.log(this.count == 0?'is empty':'!empty');
    return this.count == 0;
  }

  // for check the size
  size() {
    console.log(this.count);
    return this.count;
  }

  // Print element in stack
  print() {
    let str = '';
    for (let i = 0; i < this.count; i++){
      str += this.item[i] + '';
    }
    return str;
  }

  clear() {
     this.item = [];
    this.count = 0;
    console.log('stack cleared');
    return this.item;
  }

}
const stack = new Stack();

stack.size();
stack.isEmpty();
stack.push(5);
stack.push(50);
stack.clear();

console.log(stack.print());
stack.peek()
stack.push(55);
stack.clear();

stack.pop()
stack.isEmpty();
stack.size();
console.log(stack.print());

stack.clear();