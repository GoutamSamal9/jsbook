function Queue() {
    collection = [];
    this.print = function () {
        console.log(collection);
    };
    this.enqueue = function (element) {
        collection.push(element);
    };
    this.dequeue = function () {
        return collection.shift();
    }
    this.front = function () {
        return collection[0]
    }
    this.size = function () {
        return collection.length
    }
    this.empty = function () {
        return (collection.length===0)
    }
}

let queue = new Queue();

queue.enqueue('a');// add element to the queue
queue.enqueue("b");
queue.enqueue("c");
queue.enqueue("d");

queue.print();

queue.dequeue(); // it will remove the first element from the array
console.log(queue.front()); // it will print the first element from the array
queue.print();
