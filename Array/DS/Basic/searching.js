// var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

// console.log(numbers.indexOf(10));
// console.log(numbers.indexOf(100));

// numbers.push(10);
// console.log(numbers.lastIndexOf(10));
// console.log(numbers.lastIndexOf(100));

// console.log(numbers);

// ECMAScript 6 – the find and findIndex methods
let newNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

// function multipleOf13(element, index, array) {
//     return (element % 13 === 0) ? true : false;
// }

// console.log(newNumbers.find(multipleOf13));

// console.log(newNumbers.findIndex(multipleOf13));

console.log(newNumbers.includes(15));
console.log(newNumbers.includes(20));


let numbers2 = [7, 6, 5, 4, 3, 2, 1];
console.log(numbers2.includes(4, 5));
