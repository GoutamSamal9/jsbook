var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

// var isEven = function (x) {
//     return (x % 2 === 0) ? true : false;
// }

// var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

// isEven();

// wep page 76 (@@ iterator)

let iterator = numbers[Symbol.iterator]();
console.log(iterator.next().value);

// entries method
let aEntries = numbers.entries(); //retrieve iterator of key/value
console.log(aEntries.next().value);

// key method
let aKey = numbers.keys(); //retrieve iterator of keys
console.log(aKey.next());

// value method
let aValue = numbers.values();
console.log(aValue.next());



